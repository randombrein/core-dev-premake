FROM registry.gitlab.com/randombrein/core-dev-opencv
MAINTAINER Evren KANALICI <evren.kanalici@nevalabs.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update && apt-get -qqy --no-install-recommends install \
    git             \
&& rm -rf /var/lib/apt/lists/*

# get premake
RUN git clone https://github.com/premake/premake-core.git \
	&& cd premake-core \
	&& git submodule update --init \
	&& make -f Bootstrap.mak linux >/dev/null 2>&1 \
	&& cp bin/release/premake5 /usr/local/bin \
	&& cd / && rm -rf premake*

